# Yeast Protein Location Predictor
![Version](https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000)
[![License: GPLv2](https://img.shields.io/badge/License-GPLv2-green.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)

> This tool can predict the protein locations of given yeast proteins. The data can either be a file with unclassified protein instances or one or more unclassified protein instances.

## Purpose
  
Classification tool for yeast protein localization. Protein Localization is where a protein resides in a cel. For example in the Cytosol or in the Mitchondria. This tool uses a Stacking model to classify each protein to their cell location. A protein needs 9 attributes in order for this to work. These attributes are described in a report that can be found here: https://bitbucket.org/jfhaaijer/javaproteinlocationpredictor/src/master/. It also contains the research behind this tool. Protein location can tell us more about the protein functions. This is important because there is still much to disover about proteins. 

## Install

Clone the repository.
```sh
git clone https://jfhaaijer@bitbucket.org/jfhaaijer/javaproteinlocationpredictor.git
```

## Usage

1. Go to the libs directory.
```sh
cd javaproteinlocationpredictor/build/libs 
```

2. Start the program in the terminal with the following command
```sh
java -jar CLI-1.0-SNAPSHOT-all.jar 
```
3. Select the option you want this can either be -f for a file, -i for an instance or -h for more information.
  
  If you want more information:
  ```sh
  java -jar CLI-1.0-SNAPSHOT-all.jar -h
  ```
  
  If you want to classify a file: 
  (Example files can be found in the testdata folder)
  
  ```sh
  java -jar CLI-1.0-SNAPSHOT-all.jar -f testdata/instance.arff
  ```
  
  If you want to classify one or more instances:
```sh
java -jar CLI-1.0-SNAPSHOT-all.jar -i 'NOP3_YEAST,0.44,0.34,0.61,0.13,FALSE,Low,0.49,0.26,NUC' 

java -jar CLI-1.0-SNAPSHOT-all.jar -i 'NOP3_YEAST,0.44,0.34,0.61,0.13,FALSE,Low,0.49,0.26,NUC' 'NSP1_YEAST,0.87,0.47,0.6,0.47,FALSE,Low,0.59,0.22,NUC'
```
## Support
For any problems or questions mail : j.f.haaijer@st.hanze.nl

## Author

 **Jildou Haaijer**

## License

This project is [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_