/*
 * Copyright (c) 2019 Jildou Haaijer.
 * Licensed under GPLv3. See LGPL.txt
 */

package nl.bioinf.jfhaaijer.ProteinLocationPredictor;

/**
 * interface that specifies which options should be provided to the tool.
 * @author jildou
 */
public interface OptionsProvider {
    /**
     * serves the file with unclassified instances to be used.
     * @return dataFile the file with data
     */
    String getFile();
}
