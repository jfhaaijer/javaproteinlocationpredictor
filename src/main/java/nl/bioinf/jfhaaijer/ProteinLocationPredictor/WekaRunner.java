/*
 * Copyright (c) 2019 Jildou Haaijer.
 * Licensed under GPLv3. See LGPL.txt
 */

package nl.bioinf.jfhaaijer.ProteinLocationPredictor;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.Stacking;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *  This class uses a weka model and a trainingsdata file to classify new yeast protein instances with their cell
 *  location.
 *
 *  @author jildou
 */
class WekaRunner {
    private final String modelFile = "testdata/models/Stacking_best.model";

    /**
     * Initialize wekarunner methods
     * @param proteinData the arff data file with unclassified instances
     */
    void start(String proteinData) {
        String datafile = "testdata/train_yeastdata.arff";
        System.out.print("\nPlease note that the classification of your yeast data may take a while. " +
                "\nThe reason for this is because the algorithm used for classification is Stacking.\n" +
                "This Stacking algorithm uses 5 other algorithms to optimize its classification.\n");
        try {
            Instances instances = loadArff(datafile);
            //printInstances(instances);
            Stacking stacking = buildClassifier(instances);
            saveClassifier(stacking);
            Stacking fromFile = loadClassifier();
            Instances unknownInstances = loadArff(proteinData);
            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            classifyNewInstance(fromFile, unknownInstances);

        } catch (Exception e) {
        }
    }

    /**
     * Method that classifies new instances with help of the classifier
     * @param function Stacking classifier
     * @param unknownInstances instances to be classified
     * @throws Exception
     */
        private void classifyNewInstance (Stacking function, Instances unknownInstances) throws Exception {
            // create copy
            Instances labeled = new Instances(unknownInstances);
            // label instances
            for (int i = 0; i < unknownInstances.numInstances(); i++) {
                double clsLabel = function.classifyInstance(unknownInstances.instance(i));

                labeled.instance(i).setClassValue(clsLabel);
            }
            System.out.println("\nNew, labeled = \n" + labeled);
        }

    /**
     * Loads the classifier
     * @return Stacking model
     * @throws Exception
     */
        private Stacking loadClassifier () throws Exception {
            // deserialize model
            return (Stacking) weka.core.SerializationHelper.read(modelFile);
        }

    /**
     * Saves stacking classifier to final modelFile
     * @param stacking
     * @throws Exception
     */
        private void saveClassifier (Stacking stacking) throws Exception {
            //post 3.5.5
            // serialize model
            weka.core.SerializationHelper.write(modelFile, stacking);

            //serialize model pre 3.5.5
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelFile));
//        oos.writeObject(stacking);
//        oos.flush();
//        oos.close();
        }

    /**
     * Builds stacking classifier with met-learner SimpleLogistic and base learners J48, NaiveBayes,
     * @param instances the instances from the trainings data
     * @return Stacking classifier
     * @throws Exception
     */
        private Stacking buildClassifier (Instances instances) throws Exception {
            System.out.print("\n    1. Building the Classifier: Stacking... \n");
            Stacking function = new Stacking();         // new instance of stacking
            SimpleLogistic sl = new SimpleLogistic();   // choose meta classifier

            function.setMetaClassifier(sl);
            System.out.print("\n    2. Set meta-learners: SimpleLogistic \n");

            J48 j48 = new J48();
            NaiveBayes nb = new NaiveBayes();
            //String[] nbOptions = { "-K"};
            //nb.setOptions(nbOptions);
            IBk ibk = new IBk();
            RandomForest rf = new RandomForest();

            // Set base-learners
            System.out.print("\n    3. Set base-learners: J48, NaiveBayes -K, SimpleLogistic, IBk and RandomForest \n");
            Classifier[] stackOptions = new Classifier[] {j48, nb, sl, ibk, rf};
            function.setClassifiers(stackOptions);

            System.out.print("\n    4. Configure classifier. \n");

            function.buildClassifier(instances);   // build classifier
            System.out.print("\n    5. ...Stacking classifier is build. \n");
            return function;
        }

    /**
     * Prints the attributes and instances from the file
     * @param instances classified instances
     */
        private void printInstances (Instances instances){
            int numAttributes = instances.numAttributes();

            for (int i = 1; i < numAttributes; i++) {
                System.out.println("attribute " + i + " = " + instances.attribute(i));
            }

            System.out.println("class index = " + instances.classIndex());
//        Enumeration<Instance> instanceEnumeration = instances.enumerateInstances();
//        while (instanceEnumeration.hasMoreElements()) {
//            Instance instance = instanceEnumeration.nextElement();
//            System.out.println("instance " + instance. + " = " + instance);
//        }

            //or
            int numInstances = instances.numInstances();
            for (int i = 0; i < numInstances; i++) {
                if (i == 5) break;
                Instance instance = instances.instance(i);
                System.out.println("instance = " + instance);
            }
        }

    /**
     * Loads an arff file and processes the data
     * @param datafile the data from the commandline args
     * @return  data
     * @throws IOException
     */
        private Instances loadArff (String datafile) throws IOException {
            try {
                DataSource source = new DataSource(datafile);
                Instances data = source.getDataSet();
                // setting class attribute if the data format does not provide this information
                // For example, the XRFF format saves the class attribute information as well
                if (data.classIndex() == -1)
                    data.setClassIndex(data.numAttributes() - 1);
                return data;
            } catch (Exception e) {
                throw new IOException("could not read from file");
            }
        }
    }

