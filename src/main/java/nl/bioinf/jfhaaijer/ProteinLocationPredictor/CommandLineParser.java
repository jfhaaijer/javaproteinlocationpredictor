/*
 * Copyright (c) 2019 Jildou Haaijer.
 * Licensed under GPLv3. See LGPL.txt
 */

package nl.bioinf.jfhaaijer.ProteinLocationPredictor;

import java.io.IOException;
import java.util.Arrays;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 *
 * @author jildou
 */
public final class CommandLineParser {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CommandLineParser() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        System.out.print("Welcome to the Yeast Protein Classifier.\n");
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);

            if (op.helpRequested()) {
                op.printHelp();
                return;
            }

            WekaRunner weka = new WekaRunner();
            FileHandler proteinData = new FileHandler();

            if (op.instanceProvided()) {
                String filePath = proteinData.InstanceFileWriter(args);
                System.out.print("\nProcessing instances written to file: "+ filePath);
                weka.start(filePath);
            }

            if (op.fileProvided()) {
                if (!args[1].startsWith("/")){
                    String proteinFile = proteinData.FileEvaluator(args[1]);
                    System.out.print("\nProcessing file: "+ proteinFile);
                    weka.start(proteinFile);

                } else {throw new IOException("File path should not begin with a '/', Please try again"); }
            }
        } catch (IllegalStateException | IOException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
