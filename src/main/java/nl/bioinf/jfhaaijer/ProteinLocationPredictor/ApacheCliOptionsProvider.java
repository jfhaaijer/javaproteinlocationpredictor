/*
 * Copyright (c) 2019 Jildou Haaijer.
 * Licensed under GPLv3. See LGPL.txt
 */

package nl.bioinf.jfhaaijer.ProteinLocationPredictor;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.*;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author jildou
 */
public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String INSTANCE = "instance";
    private static final String FILE = "file";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option instanceOption = new Option("i", INSTANCE, true,
                "Argument(s) contains one or more unclassified protein instances." +
                "\nFor example: " +
                        "\n\"ADT3_YEAST,0.64,0.22,0.49,0.15,FALSE,Low,0.53,0.22,?\"." +
                        "\n(Note: instances should be quoted)");
        Option fileOption = new Option("f", FILE, true,
                "Argument contains path of an arff file with unclassified instances." +
                "\nFor example: " +
                "\ntestdata/instance.arff" +
                        "\n(Note: Only one file per run)");
        options.addOption(helpOption);
        options.addOption(instanceOption);
        options.addOption(fileOption);
    }

    /**
     * processes the command line arguments.
     * @return
     */
    private CommandLine processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            if (this.clArguments.length == 0) {
                throw new IllegalArgumentException("\nWelcome to the Protein Location Predictor. " +
                        "\nPlease select an option. \nTry: java -jar -h for more information.");
            }

        }catch(ParseException| IllegalArgumentException ex) {
            System.out.print(ex.getMessage());

            //ex.printStackTrace();
        }
        return this.commandLine;
    }


    /**
     * @return TRUE when a file is provided.
     */
    boolean fileProvided() { return this.commandLine.hasOption(FILE); }


    /**
     * @return TRUE when an instance is provided.
     */
    boolean instanceProvided() { return this.commandLine.hasOption(INSTANCE); }

    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("OptionHelpTool", options);
    }

    @Override
    public String getFile() {
        return this.commandLine.getOptionValue(FILE);
    }
}

