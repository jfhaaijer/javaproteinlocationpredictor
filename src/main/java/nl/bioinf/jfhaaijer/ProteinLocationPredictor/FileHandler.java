/*
 * Copyright (c) 2019 Jildou Haaijer.
 * Licensed under GPLv3. See LGPL.txt
 */

package nl.bioinf.jfhaaijer.ProteinLocationPredictor;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * This class writes a file when an instance is provided and checks if it has the correct attribute types.
 * It can change the a numeric attribute to a nominal attribute of  either an instance or a file.
 * @author jildou
 */
class FileHandler {

    /**
     * Writes a instances to an arff file.
     * @param Instances instances from commandline arguments or from file
     * @return testdata/instance.arff
     * @throws IOException
     */
    String InstanceFileWriter(String[] Instances) throws IOException {
        String newFile = "testdata/instance.arff";
        List<String> instanceList = new ArrayList<>(Arrays.asList(Instances));
        if (instanceList.get(0).length() < 3){  // remove the -i option from the list
            instanceList.remove(0);}
        ArrayList<String> names = new ArrayList<>();

        for(int i=0;i<Instances.length;i++) {
            if (Instances[i].length() < 3){
                i++;}
            String[] arrOfInst = Instances[i].split(",");
            names.add(arrOfInst[0]);}

        FileWriter fileWriter = new FileWriter(newFile);
        // Write header
        fileWriter.write(String.format("@relation instance\n" +
                        "\n@attribute Name {%s}\n" +
                        "@attribute mcg numeric\n" +
                        "@attribute gvh numeric\n" +
                        "@attribute alm numeric\n" +
                        "@attribute mit numeric\n" +
                        "@attribute erl {FALSE,TRUE}\n" +
                        "@attribute pox {Low,Medium,High}\n" +
                        "@attribute vac numeric\n" +
                        "@attribute nuc numeric\n" +
                        "@attribute Class {CYT,ERL,EXC,ME1,ME2,ME3,MIT,NUC,POX,VAC}\n" +
                        "\n@data\n", names.toString().replace("]", "").replace("[","")));

        for(String instance:instanceList) { fileWriter.write(InstanceEvaluator(instance)+"\n");}
        fileWriter.close();
        return newFile;
    }

    /**
     * @param instance
     * Attributes 5 & 6 should be nominal, if this is not the case use this method to change them.
     * @return instance with correct attribute types
     */
    private String InstanceEvaluator(String instance){
        String[] arrOfInstance = instance.split(",");

        if (arrOfInstance[5].matches("0.5|1")){
            if (arrOfInstance[5].matches("1")){
                arrOfInstance[5]="TRUE";
            } else arrOfInstance[5] = "FALSE";
        }

        if (arrOfInstance[6].matches("0.5|0.83|[0-9]|0.0")){
        switch (arrOfInstance[6]) {
            case "0.5":
                arrOfInstance[6] ="Medium";
                break;
            case "0.83":
                arrOfInstance[6] = "High";
                break;
            default:
                arrOfInstance[6] = "Low";  // Default is Low because most proteins have a low pox value (value of 0)
        }}
        return Arrays.toString(arrOfInstance).replace("[", "").replace("]", "");
    }

    /**
     * Checks if a file has the correct attributes, if not correct them with InstanceEvaluator
     * @param file filepath from commandline args
     * @return file or InstanceFileWriter(file) if it has wrong attributes
     * @throws IOException
     */
    String FileEvaluator(String file) throws IOException {
        List<String> instanceList = new ArrayList<>();
        Boolean rightAttributesType = Boolean.TRUE;
        Scanner sc = new Scanner(new File(file));
            while (sc.hasNext()) {
                String str = sc.nextLine();
                if (!str.startsWith("@") && str.contains("_YEAST")){
                    instanceList.add(str.replace(" ", ""));
                    String[] arrOfInstance = str.split(",");
                        if ((!arrOfInstance[6].replace(" ", "").matches("Low|Medium|High")) |
                                (!arrOfInstance[5].replace(" ", "").matches("TRUE|FALSE"))) {
                            rightAttributesType = Boolean.FALSE;
                        }
                }
            }

            if (rightAttributesType == Boolean.FALSE){
                String[] instanceArr = new String[instanceList.size()];
                instanceArr = instanceList.toArray(instanceArr);
                return InstanceFileWriter(instanceArr);
            } else {
                return file;
            }
    }
}

